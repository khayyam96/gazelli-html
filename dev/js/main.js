
// ------------ counter scripts -------------- //
if ($(".counter-wrap").length) {
    //viewport =====================
    var r = !($.fn.isInViewport = function () {
      var t = $(this).offset().top,
        e = t + $(this).outerHeight(),
        o = $(window).scrollTop(),
        n = o + $(window).height();
      return 800 < $(window).width() ? e - $(this).outerHeight() > o && t + $(this).outerHeight() < n : o < e && t < n;
    });
    //======================
    $(window).on("scroll", function () {
      if ($(".counter-wrap").length && $(".counter-wrap").isInViewport() && !r) {
        $(".counter").each(function () {
          var $this = $(this),
            countTo = $this.attr("data-count");
          $({ countNum: $this.text() }).animate({
            countNum: countTo
          },
            {
              duration: 4000,
              easing: "linear",
              step: function () {
                $this.text(Math.floor(this.countNum));
              },
              complete: function () {
                $this.text(this.countNum);
                //alert("finished");
              }
            });
        });
        r = !0;
      }

    });
  }
 
  $('.events-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    navText: ["<img src='img/carnext.svg'>",
    "<img src='img/carnext.svg'>"],
    autoplay: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});


$(document).ready(function(){
  $("#hide").click(function(){
    $("#alert").hide();
  });
  
});

$(document).ready(function(){
  $("#show").click(function(){
    $("#pop-up-login").show();
  });
  $("#close-btn").click(function(){
    $("#pop-up-login").hide();
  });
});

$(document).ready(function(){
  $("#open-menu").click(function(){
    $("#mobil-menu").show();
  });
  $("#close-menu").click(function(){
    $("#mobil-menu").hide();
  });
});


$(document).ready(function(){
  $('.checkbox-slide input').on("change", function() {
      if ($(this).is(':checked')) {
        $(this).parent().css({'background-color':'#b2282b'});
      } else {
        $(this).parent().css({'background-color':'#ddd'});
      }
  });
});



$('.banner-carousel').owlCarousel({
  loop:true,
  // autoplay:true,
  dots: true,
  // autoplayTimeout:4450,
  // animate: true,
  nav:true,
  navText: ["<img src='myprevimage.png'>","<img src='mynextimage.png'>"],
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
});

// $('.banner-carousel').on('initialized.owl.carousel changed.owl.carousel', function(e) {
//   if (!e.namespace)  {
//     return;
//   }
//   var carousel = e.relatedTarget;
//   $('.slider-counter').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
// }).owlCarousel({
//   items: 1,
//   loop:true,
//   autoplay:true,
//   dots: true,
//   autoplayTimeout:4500,
//   animate: true,
//   margin:0,
//   nav:true
// });




//mobil carousel

$('.mobil_carusel').owlCarousel({
  loop:true,
  margin:10,
  stagePadding:50,
  lazyLoad:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      575:{
          items:1
      },
      1000:{
          items:1
      }
  }
});

$('.mobil_carusel1').owlCarousel({
  loop:true,
  margin:10,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      575:{
          items:1
      },
      1000:{
          items:1
      }
  }
});

//news carousel

$('.news-carousel').owlCarousel({
  loop:true,
  //margin:10,
  autoplay:true,
  autoplayTimeout:3000,
  autoplayHoverPause:true,
  nav:true,
  navText: ["<img src='/img/newsarrowleft.svg'>", "<img src='/img/newsarrow.svg'>"],
  responsive:{
      0:{
          items:1
      },
      575:{
          items:1
      },
      1000:{
          items:1
      }
  }
});



$('.cabinet-mobil-list').owlCarousel({
  loop:true,
  margin:20,
  // autoplay:true,
  // autoplayTimeout:3000,
  // autoplayHoverPause:true,
  nav:true,
  responsive:{
      0:{
          items:4
      },
      575:{
          items:4
      },
      1000:{
          items:1
      }
  }
});

// function loginmobil(){
//   console.log("this");
//   document.getElementById('dropdown').style.height='100px';
// }


function openfilter(){
  document.getElementById('book-choice').style.display='block';
}

//questions js
function openmobilmenu(){

    document.getElementById('nav').style.right='0%';
  }
  function closemobilmenu(){
    document.getElementById('nav').style.right='-200%';
  }
  function resultopen(){
        document.getElementById('result').style.display="block";
  }

  function openProjectPopup(){
        document.getElementById('project-popup').style.display="block";
  }
  $('.team_member').click(function(){
    var $trigger = $(this).data('team_member');
    $('.our-team-popup').each(function(){
        var $popup_trigger = $(this).data('team_modal');
        if($trigger == $popup_trigger){
          $(this).show().siblings('.our-team-popup').hide();
        }
    });
  });
  $('.close_icon').click(function(){
    $('.pop_up').hide();
  });

  $('.pop_up').click(function(){
    $('.pop_up').hide();
  });

//   function openteampopup(){
//     document.getElementById('teampopup').style.display="block";
// }
    
//   function closepopupteam(){
    
//     document.getElementById('teampopup').style.display="none";
// }

// var mw = document.getElementsByClassName('accardion');
// var head = document.getElementsByClassName('head');
// for (i = 0; i < head.length; i++) {
//     head[i].addEventListener('click', toggleItem, false);
// }
// function toggleItem() {
//     var itemClass = this.parentNode.className;
//     for (i = 0; i < mw.length; i++) {
//         mw[i].className = 'accardion close';
//     }
//     if (itemClass == 'accardion close') {
//         this.parentNode.className = 'accardion open';
//     }
// }

//mobil menu js

var mobilmenu = document.getElementsByClassName('mobil-menu');
var head1 = document.getElementsByClassName('head');
for (i = 0; i < head1.length; i++) {
    head1[i].addEventListener('click', toggleItem, false);
}
function toggleItem() {
    var itemClass = this.parentNode.className;
    for (i = 0; i < mobilmenu.length; i++) {
      mobilmenu[i].className = 'mobil-menu close';
    }
    if (itemClass == 'mobil-menu close') {
        this.parentNode.className = 'mobil-menu open';
    }
}


  $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });


  //search js

  function opensearch(){
    console.log("this");
    document.getElementById('search').style.right='0%';
  }
  function closesearch(){
    document.getElementById('search').style.right='-200%';
  }

//-----wow.js--/
new WOW().init();

//owl js
function openmobilfilter(){
  console.log("this");
  document.getElementById('mobil-filter-block').style.display='block';
}

function closefilterbloc(){
  console.log("this");
  document.getElementById('mobil-filter-block').style.display='none';
}

function registirFinish(){
  console.log("this");
  document.getElementById('registration-popup').style.display='block';
}

function closepopup(){
  console.log("this");
  document.getElementById('registration-popup').style.display='none';
}


function openlogin(){
    console.log("this");
    document.getElementById('loginmenu').style.height='auto';
    style.padding='16px 0';
  }


//custom select js
  $(".custom-select").each(function() {
    var classes = $(this).attr("class"),
        id      = $(this).attr("id"),
        name    = $(this).attr("name");
    var template =  '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(this).find("option").each(function() {
          template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
    template += '</div></div>';
    
    $(this).wrap('<div class="custom-select-wrapper"></div>');
    $(this).hide();
    $(this).after(template);
  });
  $(".custom-option:first-of-type").hover(function() {
    $(this).parents(".custom-options").addClass("option-hover");
  }, function() {
    $(this).parents(".custom-options").removeClass("option-hover");
  });
  $(".custom-select-trigger").on("click", function() {
    $('html').one('click',function() {
      $(".custom-select").removeClass("opened");
    });
    $(this).parents(".custom-select").toggleClass("opened");
    event.stopPropagation();
  });
  $(".custom-option").on("click", function() {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
  });

//custm select


// var music = document.getElementById('music'); // id for audio element
// var duration = music.duration; // Duration of audio clip, calculated here for embedding purposes
// var pButton = document.getElementById('pButton'); // play button
// var playhead = document.getElementById('playhead'); // playhead
// var timeline = document.getElementById('timeline'); // timeline

// timeline width adjusted for playhead
// var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;

// // play button event listenter
// pButton.addEventListener("click", play);

// // timeupdate event listener
// music.addEventListener("timeupdate", timeUpdate, false);

// // makes timeline clickable
// timeline.addEventListener("click", function(event) {
//     moveplayhead(event);
//     music.currentTime = duration * clickPercent(event);
// }, false);

// // returns click as decimal (.77) of the total timelineWidth
// function clickPercent(event) {
//     return (event.clientX - getPosition(timeline)) / timelineWidth;
// }

// makes playhead draggable
// playhead.addEventListener('mousedown', mouseDown, false);
// window.addEventListener('mouseup', mouseUp, false);

// Boolean value so that audio position is updated only when the playhead is released
// var onplayhead = false;

// // mouseDown EventListener
// function mouseDown() {
//     onplayhead = true;
//     window.addEventListener('mousemove', moveplayhead, true);
//     music.removeEventListener('timeupdate', timeUpdate, false);
// }

// mouseUp EventListener
// getting input from all mouse clicks
// function mouseUp(event) {
//     if (onplayhead == true) {
//         moveplayhead(event);
//         window.removeEventListener('mousemove', moveplayhead, true);
//         // change current time
//         music.currentTime = duration * clickPercent(event);
//         music.addEventListener('timeupdate', timeUpdate, false);
//     }
//     onplayhead = false;
// }
// mousemove EventListener
// Moves playhead as user drags
// function moveplayhead(event) {
//     var newMargLeft = event.clientX - getPosition(timeline);

//     if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
//         playhead.style.marginLeft = newMargLeft + "px";
//     }
//     if (newMargLeft < 0) {
//         playhead.style.marginLeft = "0px";
//     }
//     if (newMargLeft > timelineWidth) {
//         playhead.style.marginLeft = timelineWidth + "px";
//     }
// }

// // timeUpdate
// // Synchronizes playhead position with current point in audio
// function timeUpdate() {
//     var playPercent = timelineWidth * (music.currentTime / duration);
//     playhead.style.marginLeft = playPercent + "px";
//     if (music.currentTime == duration) {
//         pButton.className = "";
//         pButton.className = "play";
//     }
// }

// //Play and Pause
// function play() {
//     // start music
//     if (music.paused) {
//         music.play();
//         // remove play, add pause
//         pButton.className = "";
//         pButton.className = "pause";
//     } else { // pause music
//         music.pause();
//         // remove pause, add play
//         pButton.className = "";
//         pButton.className = "play";
//     }
// }

// // Gets audio file duration
// music.addEventListener("canplaythrough", function() {
//     duration = music.duration;
// }, false);

// // getPosition
// // Returns elements left position relative to top-left of viewport
// function getPosition(el) {
//     return el.getBoundingClientRect().left;
// }





$(document).ready(function(){
  if($('.book_item').length){
  var audio = document.getElementById('audio-track');
  var progress = document.querySelector('.progress');
  var progress_bar = document.querySelector('.progress-bar');
  var play_pause = document.getElementById('play-pause');
  var start_time = document.querySelector('.start-time');
  var end_time = document.querySelector('.end-time');
  var progressbarWidth = progress_bar.offsetWidth;
  var $switch = false;
  var percent;
  
  play_pause.addEventListener('click',function(){
      if(!$switch){
          audio.play();
          play_pause.innerHTML= '<img src="img/pause.svg" alt="">';
          $switch = true;
      }else{
          audio.pause();
          play_pause.innerHTML = ' <img src="play.svg" alt="">';
          $switch = false;
      }
  });



  audio.addEventListener('timeupdate',function(){
      progress.style.width = audio.currentTime / audio.duration * 100 + "%";
      var audio_length = audio.duration - audio.currentTime;
      var minutes = parseInt(audio_length / 60, 10);
      var seconds = parseInt(audio_length % 60);
      if(seconds< 10){
          seconds = '0' + String(seconds);
      }
      end_time.innerHTML = minutes + ':' + seconds;

      var mins = Math.floor(audio.currentTime / 60);
      
      if (mins < 10) {
          mins =  + String(mins);
      }
          var secs = Math.floor(audio.currentTime % 60);
      if (secs < 10) {
          secs = '0' + String(secs);
      }

      start_time.innerHTML = mins + ':' + secs;
      
      if(this.currentTime >= this.duration){
          $switch = false;
          audio.pause();
          play_pause.innerHTML = ' <img src="play.svg" alt="">';
      }
  });

  progress_bar.addEventListener('click', seeking);
}
  function seeking(e) {
      if(progressbarWidth != undefined){
           percent = e.offsetX / progressbarWidth;
      }else{
           percent = e.offsetX;
      }
      audio.currentTime = percent * audio.duration;
  }
});
























$('.book-carousel').owlCarousel({
  loop:true,
  margin:10,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:3
      },
      1000:{
          items:1
      }
  }
});

if($('.glo-count').length){
  var count  = 1;
  $('.decrease-count').click(function(e){
    e.preventDefault();
    if(count > 1){
      count --;
    }else{
      count = 1;
    }
    $('.count').text(count);
  });
  $('.increase-count').click(function(e){
    e.preventDefault();
    if(count <= 7){
      count ++;
    }
    $('.count').text(count);
  });
  
}

var self = $("#masonry");
self.imagesLoaded(function () {
    self.masonry({
        gutterWidth: 15,
        isAnimated: true,
        itemSelector: ".card-container"
    });
});
$(".filters .btn").click(function(e) {
    e.preventDefault();
    var filter = $(this).attr("data-filter");
    self.masonryFilter({
        filter: function () {
            if (!filter) return true;
            return $(this).attr("data-filter") == filter;
        }
    });
});